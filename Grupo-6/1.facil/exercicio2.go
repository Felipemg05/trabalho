package main

import "fmt"

func main() {
    var x float64
    var y float64
    var z float64
    var media float64
  fmt.Println("Digite três números")
  fmt.Scan(&x, &y, &z)
  
  media = (x + y + z) / 3
  
  fmt.Println("A média dos três números é:", media)
}
