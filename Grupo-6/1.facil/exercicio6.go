package main

import "fmt"

func main() {
  var x int
  for x = 10; x >= 1; x-- {
     fmt.Println(x)
  }
}
