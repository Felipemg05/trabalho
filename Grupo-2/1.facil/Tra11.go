package main

import "fmt"

func nPrimo(numero int) bool {
	if numero <= 1 {
		return false
	}
	for i := 2; i <= numero/2; i++ {
		if numero%i == 0 {
			return false
		}
	}
	return true
}

func main() {
	var numero int
	fmt.Println("Digite um número : ")
	fmt.Scan(&numero)

	if nPrimo(numero) {
		fmt.Println(numero, "é um número primo.")
	} else {
		fmt.Println(numero, "não é um número primo.")
	}
}
