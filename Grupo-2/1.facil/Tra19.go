package main

import (
	"fmt"
	"math"
)

func main() {

	var raio float64

	fmt.Println("Digite o raio da esfera")
	fmt.Scan(&raio)

	volume := ((4 * 3.1415) * (math.Pow(raio, 2))) / 2
	fmt.Printf("O vulome da esfera %f", volume)

}
