package main

import (
	"fmt"
)

func main() {
	var frase string
	fmt.Println("Digite uma frase em minúsuculo:")
	fmt.Scanln(&frase)

	contador := 0

	for _, char := range frase {
		switch char {
		case 'a', 'e', 'i', 'o', 'u':
			contador++
		}
	}

	fmt.Printf("A frase contém %d vogais.\n", contador)
}
