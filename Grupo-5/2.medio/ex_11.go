package main

import "fmt"

func main() {
	var a, b, c float64
	fmt.Print("Dê-me o primeiro lado do triângulo: ")
	fmt.Scan(&a)
	fmt.Print("Dê-me o segundo lado do triângulo: ")
	fmt.Scan(&b)
	fmt.Print("Dê-me o terceiro lado do triângulo: ")
	fmt.Scan(&c)
	if a < (b+c) && b < (a+c) && c < (a+b) {
		fmt.Println("O triângulo é válido!")
	} else {
		fmt.Println("Triângulo inválido!")
	}
}
