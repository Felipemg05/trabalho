package main

import "fmt"

func calcMedia(valores []float64) float64 {
	total := 0.0
	for _, num := range valores {
		total += num
	}
	return total / float64(len(valores))
}

func main() {
	var valores []float64
	var numero float64
	var entrada string

	fmt.Println("Digite as idades \npara concluir, digite 'b': ")

	for {
		_, err := fmt.Scan(&entrada)
		if err != nil {
			fmt.Println("Erro ao ler as idades.")
			return
		}

		if entrada == "b" {
			break
		}

		_, err = fmt.Sscanf(entrada, "%f", &numero)
		if err != nil {
			fmt.Println("ERRO! Digite uma idade válida.")
			continue
		}

		valores = append(valores, numero)
	}

	if len(valores) == 0 {
		fmt.Println("Sem valores para calcular a média de idades.")
		return
	}

	media := calcMedia(valores)
	fmt.Printf("media de idades: %.2f\n", media)
}
