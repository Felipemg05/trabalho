package main

import "fmt"

func main() {
	var a int
	fmt.Print("informe um ano qualquer para saber se o ano é bissexto:")
	fmt.Scan(&a)
	if a%4 == 0 {
		fmt.Print("o ano informado é bissexto")
	} else {
		fmt.Print("o ano informado não é bissexto")
	}
}
