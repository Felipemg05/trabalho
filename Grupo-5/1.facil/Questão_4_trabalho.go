package main

import "fmt"

func main() {
	var num int
	r := 1
	fmt.Print("informe um numero para calcular seu fatorial: ")
	fmt.Scan(&num)
	for i := num; i >= 1; i-- {
		r = r * i
	}
	fmt.Printf("o fatorial de %d é %d", num, r)
}
